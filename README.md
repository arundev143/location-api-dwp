# Location API

## Getting Started
Below are some instructions, which will get the project up and running on your local machine for development and testing purposes.

### Prerequisites
* [Maven 3.6.1](https://maven.apache.org/)
* [openjdk 11.0.9 (Java 11)](https://openjdk.java.net/)
* [Docker](https://www.docker.com/)
 
## Building & Running
There are multiple steps to building / compiling and running this project:
* Build / compile the jar using Maven
 
### Building jar
Before you run all services you will need to compile and build the service .jar. To do this run the following command:
```
mvn clean install -Dmaven.test.skip
```
This will also run any tests as well as any code quality checks.
 
### Running the application as a Docker container using docker-compose
To run the API, run the following command in the workspace
```
docker-compose -f location-api-docker-compose.yml up
```
To stop the API run the following command in the workspace
```
docker-compose -f location-api-docker-compose.yml down
```

### How to deploy and get response from api - running from local-machine as a docker container
There are two endpoints in this application
 1) Endpoint returns the data of Both users who choose london as their location and users close to 50 Miles radius of london
````
http://localhost:8082/v1/location
````
IMPORTANT - You have to pass content type in header, else it will not process the request.
######Http status : 200


 2. Endpoint return only users choose london as their location.
````
http://localhost:8082/v1/location?city=London
````
IMPORTANT - You have to pass content type in header, else it will not process the request.

######Http status : 200

 3) Endpoint return 404 error (Not found) for any city other than London.
````
http://localhost:8082/v1/location?city=Bristol
````
IMPORTANT - You have to pass content type in header, else it will not process the request.

######Http status : 404

## Running Integration Test
For Integration test we have to use another profile -> **docker-integration-tests**
 ```
mvn clean -P docker-integration-tests post-integration-test
 ```
We must use the **post-integration-test**, it will remove the containers after the run. If you want to debug the containers for any issue better to run **integration-test**  instead, which will keep the containers without deleting after the test, so that manually we can run the containers again and test.


## Invoking Wiremock Server locally
````
docker-compose -f wiremock-docker-compose.yml up
````
## Shutdown Wiremock Server locally
````
docker-compose -f wiremock-docker-compose.yml down
````

## For execute `checkstyle` plugin before pushing code to remote

````
mvn -Dcheckstyle.violationSeverity=error checkstyle:check
````


