FROM adoptopenjdk/openjdk11:jre-11.0.11_9-alpine@sha256:732e076ad73b3859b2a691a8bede7397ac61f179b8d9ff386c76fa7a33b8ca9e

LABEL maintainer="arundev143@gmail.com"

ENV APPLICATION_USER=app \
    APPLICATION_GROUP=app_group \
    APPLICATION_PATH=/app.jar

EXPOSE 8082 9321

COPY wrapper.sh /wrapper.sh


RUN addgroup --gid 10001 ${APPLICATION_GROUP} && \
    adduser --uid 10002 --gecos "" --disabled-password --ingroup ${APPLICATION_GROUP} ${APPLICATION_USER} && \
    chmod 555 /wrapper.sh

ENTRYPOINT ["/wrapper.sh"]

COPY target/*.jar ${APPLICATION_PATH}
