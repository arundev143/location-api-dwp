FROM adoptopenjdk/openjdk11:jre-11.0.11_9-alpine
USER root

ARG buildVersion=0.0.1-SNAPSHOT
ARG springProfile=default
ARG exposedPort=8082
ARG serviceName=location-api
ARG protocol=http

ENV springProfileEnv=$springProfile
ENV exposedPortEnv=$exposedPort
ENV serviceNameEnv=$serviceName
ENV protocolEnv=$protocol


WORKDIR /service

COPY target/${serviceName}-${buildVersion}.jar /service/${serviceName}.jar

EXPOSE $exposedPort

ENTRYPOINT java -Dspring.profiles.active=$springProfileEnv -jar ./${serviceNameEnv}.jar

# Run wget at a regular interval to check that the service is healthy.
HEALTHCHECK --interval=5s CMD ["sh", "-c", "wget --no-check-certificate http://localhost:9321/actuator/health -O /dev/null"]
