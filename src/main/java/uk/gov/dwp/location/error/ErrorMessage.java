package uk.gov.dwp.location.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorMessage {

    /** title need to be used in response when the error response body empty from rest call. */
    EMPTY_BODY_ERROR_TITLE("Error Response Body is empty"),

    /** message/details need to be used in response when the error response body empty from rest call. */
    EMPTY_BODY_ERROR_MSG("Actual exception message also empty"),

    /** Template for TITLE - Default. %s replace with service name ( failed service ). */
    DEFAULT_TITLE_TEMPLATE("Error not mapped in application - %s"),

    /** Template for MESSAGE - Default. %s replace with service name ( failed service ). */
    DEFAULT_MESSAGE_TEMPLATE("Actual error message is empty - %s");

    /** attribute to store the message. */
    private final String msg;
}
