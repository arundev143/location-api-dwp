package uk.gov.dwp.location.error;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.enums.ApplicationErrorCodes;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ErrorCodesConfig {

    @Bean
    public Map<String, Integer> errorMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.NOT_FOUND.name(),
                ApplicationErrorCodes.USER_SERVICE_NOT_FOUND.getErrorCode());
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.BAD_REQUEST.name(),
                ApplicationErrorCodes.USER_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.SERVICE_UNAVAILABLE.name(),
                ApplicationErrorCodes.USER_SERVICE_UNAVAILABLE.getErrorCode());
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.UNAUTHORIZED.name(),
                ApplicationErrorCodes.USER_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.FORBIDDEN.name(),
                ApplicationErrorCodes.USER_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.USER_LIST.name() + HttpStatus.INTERNAL_SERVER_ERROR.name(),
                ApplicationErrorCodes.USER_SERVICE_UNAVAILABLE.getErrorCode());


        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.NOT_FOUND.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_NOT_FOUND.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.BAD_REQUEST.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.FORBIDDEN.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.UNAUTHORIZED.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.BAD_REQUEST.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_BAD_REQUEST.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.SERVICE_UNAVAILABLE.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_UNAVAILABLE.getErrorCode());
        map.put(ServiceEnum.LONDON_USERS.name() + HttpStatus.INTERNAL_SERVER_ERROR.name(),
                ApplicationErrorCodes.LONDON_USERS_SERVICE_UNAVAILABLE.getErrorCode());

        return map;
    }
}
