package uk.gov.dwp.location.error;

public class ServiceException extends RuntimeException {
    public ServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
