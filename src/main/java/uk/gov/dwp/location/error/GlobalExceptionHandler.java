package uk.gov.dwp.location.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uk.gov.dwp.location.openapi.model.ErrorDetails;
import uk.gov.dwp.location.utils.ExceptionHandlerUtils;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * The response content type.
     */
    public static final String CONTENT_TYPE = "application/json";

    @ExceptionHandler(value = {LocationApiException.class})
    public ResponseEntity<ErrorDetails> handleLocationAppExceptionException(final LocationApiException exception,
                                                                            final HttpServletRequest request) {
        final ErrorDetails errorDetails = createErrorDetails(
                exception.getErrorCode(),
                exception.getMessage(),
                exception.getTitle(),
                HttpStatus.valueOf(exception.getHttpStatusCode()));

        return new ResponseEntity<>(errorDetails, getResponseHeaders(request.getRequestURI()),
                HttpStatus.valueOf(exception.getHttpStatusCode()));
    }

    private ErrorDetails createErrorDetails(
            final Integer code,
            final String message,
            final String title,
            final HttpStatus httpStatus) {
        return new ErrorDetails().addErrorsItem(
                ExceptionHandlerUtils.buildErrorDetails(
                        code,
                        message,
                        title,
                        httpStatus)
        );
    }


    public static HttpHeaders getResponseHeaders(final String uri) {
        HttpHeaders headers = new HttpHeaders();
        String responseContentType;
        responseContentType = CONTENT_TYPE;
        headers.add("Content-Type", responseContentType);
        return headers;
    }
}
