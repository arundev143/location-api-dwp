package uk.gov.dwp.location.error;

import lombok.Getter;
import lombok.Setter;
import uk.gov.dwp.location.dto.ErrorDto;

@Getter
@Setter
public class LocationApiException extends RuntimeException {


    /** The https status code. */
    private final int httpStatusCode;

    /** The application error code provided. */
    private final Integer errorCode;

    /** The error field name. */
    private final String message;

    /** Title of the exception. */
    private final String title;

    /**
     * attribute for storing  service name.
     */
    private String serviceName;

    public LocationApiException(final ErrorDto errorDto) {
        super(errorDto.getMessage());
        this.httpStatusCode = errorDto.getStatusCode();
        this.title = errorDto.getTitle();
        this.message = errorDto.getMessage();
        this.serviceName = errorDto.getServiceName();
        this.errorCode = errorDto.getErrorCode();
    }
}
