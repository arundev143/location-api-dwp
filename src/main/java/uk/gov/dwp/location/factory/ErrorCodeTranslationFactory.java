package uk.gov.dwp.location.factory;

import org.springframework.stereotype.Component;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.error.ServiceException;
import uk.gov.dwp.location.service.ErrorCodeTranslationService;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
public class ErrorCodeTranslationFactory {


    /**
     * The errorCodeTranslationServiceMap map.
     */
    private Map<ServiceEnum, ErrorCodeTranslationService> errorCodeTranslationServiceMap;

    /**
     * Constructor.
     *
     * @param errorCodeTranslationServiceSet - set of errorCodeTranslationService
     */
    public ErrorCodeTranslationFactory(final Set<ErrorCodeTranslationService> errorCodeTranslationServiceSet) {
        createErrorCodeTranslationServiceMap(errorCodeTranslationServiceSet);
    }

    /**
     * getErrorCodeTranslationService method.
     *
     * @param serviceEnum - service enum
     * @return ErrorCodeTranslationService
     */
    public ErrorCodeTranslationService getErrorCodeTranslationService(final ServiceEnum serviceEnum) {
        return Optional.ofNullable(errorCodeTranslationServiceMap.get(serviceEnum))
                .orElseThrow(() -> new ServiceException("Error code translation service not found", new Exception()));
    }

    /**
     * createErrorCodeTranslationServiceMap method.
     * @param errorCodeTranslationServiceSet - errorCodeTranslationServiceSet.
     */
    private void createErrorCodeTranslationServiceMap(final Set<ErrorCodeTranslationService> errorCodeTranslationServiceSet) {
        errorCodeTranslationServiceMap = new EnumMap<>(ServiceEnum.class);
        errorCodeTranslationServiceSet.forEach(errorCodeTranslationService ->
                errorCodeTranslationServiceMap.put(errorCodeTranslationService.getServiceEnum(), errorCodeTranslationService));
    }
}

