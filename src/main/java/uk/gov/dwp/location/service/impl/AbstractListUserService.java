package uk.gov.dwp.location.service.impl;

import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;
import uk.gov.dwp.location.dto.ErrorDto;
import uk.gov.dwp.location.enums.ErrorTypes;
import uk.gov.dwp.location.error.LocationApiException;
import uk.gov.dwp.location.service.ErrorCodeTranslationService;

public abstract class AbstractListUserService {
    /**
     * Method will create and throw the client errors.
     * @param response - response from the web call.
     * @param errorCodeTranslationService - errorCodeTranslationService.
     * @return Throwable
     */
    public Mono<? extends Throwable> onStatus4XXCallBack(final ClientResponse response,
                                                         final ErrorCodeTranslationService errorCodeTranslationService) {
        final ErrorDto errorDto = ErrorDto.builder().errorCode(response.statusCode().value())
                .message(response.statusCode().name())
                .serviceName(errorCodeTranslationService.getServiceEnum().name())
                .errorCode(errorCodeTranslationService.getErrorCode(response.statusCode()))
                .statusCode(response.statusCode().value())
                .title("Error occurred while retrieving the User List")
                .build();
        ErrorTypes.error(response.statusCode().name()).setErrorContext(errorDto);
        throw new LocationApiException(errorDto);
    }
}
