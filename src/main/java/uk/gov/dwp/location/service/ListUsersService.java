package uk.gov.dwp.location.service;

import uk.gov.dwp.location.openapi.model.User;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ListUsersService {
    CompletableFuture<List<User>> getUserList();
}
