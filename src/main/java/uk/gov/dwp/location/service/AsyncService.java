package uk.gov.dwp.location.service;

import org.springframework.http.ResponseEntity;
import uk.gov.dwp.location.openapi.model.LocationResponseJSONAPI;

public interface AsyncService {

    ResponseEntity<LocationResponseJSONAPI> getUsersList(final String city);

}
