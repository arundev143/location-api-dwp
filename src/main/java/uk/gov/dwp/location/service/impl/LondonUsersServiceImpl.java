package uk.gov.dwp.location.service.impl;

import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.factory.ErrorCodeTranslationFactory;
import uk.gov.dwp.location.openapi.model.User;
import uk.gov.dwp.location.service.ListUsersService;

import javax.annotation.CheckForNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Component
@Qualifier("listLondonUsers")
public class LondonUsersServiceImpl extends AbstractListUserService implements ListUsersService {

    /**
     * Url for retrieve users list.
     */
    @Value("${application.endpoints.city-users-service-url}")
    private String londonUsersUrl;

    /**
     * We are only searching with london city.
     */
    private static final String CITY = "London";

    /**
     * The web client.
     */
    private final WebClient webClient;

    /**
     * Factory for providing error translation.
     */
    private final ErrorCodeTranslationFactory errorCodeTranslationFactory;

    /**
     * Constructor.
     *
     * @param webClient                   - webclient object.
     * @param errorCodeTranslationFactory - factory object.
     */
    public LondonUsersServiceImpl(final WebClient webClient, final ErrorCodeTranslationFactory errorCodeTranslationFactory) {
        this.webClient = webClient;
        this.errorCodeTranslationFactory = errorCodeTranslationFactory;

    }

    /**
     * Method will collect the users who lives in London.
     *
     * @return CompletableFuture<List < User>>
     */
    @Override
    @CheckForNull
    public CompletableFuture<List<User>> getUserList() {
        final User[] users = webClient.get()
                .uri(String.format(londonUsersUrl, CITY))
                .headers(httpHeaders -> httpHeaders.add(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, response -> onStatus4XXCallBack(response,
                        errorCodeTranslationFactory.getErrorCodeTranslationService(ServiceEnum.LONDON_USERS)))
                .bodyToMono(User[].class)
                .block();
        return CompletableFuture.completedFuture(Arrays.asList(Optional.ofNullable(users).orElse(new User[]{})));

    }
}
