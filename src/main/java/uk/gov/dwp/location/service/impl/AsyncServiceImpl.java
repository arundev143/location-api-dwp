package uk.gov.dwp.location.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uk.gov.dwp.location.dto.ErrorDto;
import uk.gov.dwp.location.enums.ApplicationErrorCodes;
import uk.gov.dwp.location.error.LocationApiException;
import uk.gov.dwp.location.openapi.model.LocationResponseJSONAPI;
import uk.gov.dwp.location.openapi.model.User;
import uk.gov.dwp.location.service.AsyncService;
import uk.gov.dwp.location.service.ListUsersService;
import uk.gov.dwp.location.utils.DistanceCalculator;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AsyncServiceImpl implements AsyncService {
    /**
     * User service object.
     */
    private final ListUsersService userService;

    /**
     * User Service Object for storing the london user service.
     */
    private final ListUsersService londonUserService;

    public AsyncServiceImpl(@Qualifier("listUsers") final ListUsersService userService,
                            @Qualifier("listLondonUsers") final ListUsersService londonUserService) {

        this.userService = userService;
        this.londonUserService = londonUserService;
    }

    /**
     * Method will return the response back to the callers.
     *
     * @return LocationResponseJSONAPI
     */
    @Override
    public ResponseEntity<LocationResponseJSONAPI> getUsersList(final String city) {
        final LocationResponseJSONAPI responseJSONAPI = new LocationResponseJSONAPI();
        try {
            extractPeopleCloseToLondon(city, responseJSONAPI);
            extractPeopleLivesInLondon(responseJSONAPI);
        } catch (Exception e) {
            log.error("ExecutionException caught - {}", e.getMessage());
            handleError(e);
        }
        log.info(responseJSONAPI.toString());
        return ResponseEntity.ok(responseJSONAPI);
    }

    /**
     * Method will return Users who declared as they lives in london.
     *
     * @param responseJSONAPI - response object
     * @throws InterruptedException                    - exception
     * @throws java.util.concurrent.ExecutionException - execution exception
     */
    private void extractPeopleLivesInLondon(final LocationResponseJSONAPI responseJSONAPI) throws InterruptedException,
            java.util.concurrent.ExecutionException {
        final CompletableFuture<List<User>> listLondonUsersFuture = londonUserService.getUserList();
        final List<User> londonUserList = listLondonUsersFuture.get();
        log.info("-------- London users  : {}", londonUserList.size());
        responseJSONAPI.setLondonBasedUserList(londonUserList);
    }

    /**
     * Method will return Users who lives close to 50 mile radius of london.
     *
     * @param city            - city passed from request.
     * @param responseJSONAPI - response object.
     * @throws InterruptedException                    - exception object
     * @throws java.util.concurrent.ExecutionException - exception object
     */
    private void extractPeopleCloseToLondon(final String city, final LocationResponseJSONAPI responseJSONAPI)
            throws InterruptedException, java.util.concurrent.ExecutionException {
        boolean isLondonInfoOnly = validateCity(city);
        if (!isLondonInfoOnly) {
            final CompletableFuture<List<User>> listOfUsersFuture = userService.getUserList();
            final List<User> userList = listOfUsersFuture.get();
            final List<User> filteredUser = getFilteredUsers(userList);
            log.info("-------- Users close to london users : {}", filteredUser.size());
            responseJSONAPI.setUserList(filteredUser);
        }
    }

    /**
     * Method will validate if the city is london otherwise throw exception.
     *
     * @param city - city.
     * @return boolean
     */
    private boolean validateCity(final String city) {
        return Optional.ofNullable(city).map(currentCity -> {
            if (currentCity.equalsIgnoreCase("London")) {
                return true;
            }
            final ErrorDto errorDto = ErrorDto.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("Wrong City " + city)
                    .title(HttpStatus.NOT_FOUND.getReasonPhrase())
                    .errorCode(ApplicationErrorCodes.WRONG_CITY.getErrorCode()).build();
            throw new LocationApiException(errorDto);
        }).orElse(false);
    }

    /**
     * Filter users based on 50 miles radius of london.
     *
     * @param userList users list
     * @return List<User>
     */
    private List<User> getFilteredUsers(final List<User> userList) {
        return userList.stream().filter(DistanceCalculator::distanceFromLondonUnder50Miles)
                .collect(Collectors.toList());
    }

    /**
     * Method that handles if any error occurs.
     *
     * @param t - throwable
     */
    private void handleError(final Throwable t) {
        if (t instanceof LocationApiException) {
            throw (LocationApiException) t;
        }
    }
}

