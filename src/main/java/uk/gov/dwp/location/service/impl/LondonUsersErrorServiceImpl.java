package uk.gov.dwp.location.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.error.ErrorCodesConfig;
import uk.gov.dwp.location.service.ErrorCodeTranslationService;

@Service
@Slf4j
public class LondonUsersErrorServiceImpl implements ErrorCodeTranslationService {

    /**
     * The errorCodesConfig.
     */
    private final ErrorCodesConfig errorCodesConfig;

    public LondonUsersErrorServiceImpl(final ErrorCodesConfig errorCodesConfig) {
        this.errorCodesConfig = errorCodesConfig;
    }

    @Override
    public Integer getErrorCode(final HttpStatus httpStatus) {
        return errorCodesConfig.errorMap().get(ServiceEnum.LONDON_USERS.name() + httpStatus.name());
    }

    @Override
    public ServiceEnum getServiceEnum() {
        return ServiceEnum.LONDON_USERS;
    }
}

