package uk.gov.dwp.location.service;

import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.enums.ServiceEnum;

public interface ErrorCodeTranslationService {
    Integer getErrorCode(HttpStatus httpStatus);
    ServiceEnum getServiceEnum();
}
