package uk.gov.dwp.location.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.factory.ErrorCodeTranslationFactory;
import uk.gov.dwp.location.openapi.model.User;
import uk.gov.dwp.location.service.ListUsersService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Component
@Qualifier("listUsers")
@Slf4j
public class ListUsersServiceImpl extends AbstractListUserService implements ListUsersService {

    /**
     * Url for retrieve users list.
     */
    @Value("${application.endpoints.users-list-service-url}")
    private String usersListServiceUrl;

    /**
     * The web client.
     */
    private final WebClient webClient;

    /**
     * Factory for providing error translation.
     */

    private final ErrorCodeTranslationFactory errorCodeTranslationFactory;

    public ListUsersServiceImpl(final WebClient webClient, final ErrorCodeTranslationFactory errorCodeTranslationFactory) {
        this.webClient = webClient;
        this.errorCodeTranslationFactory = errorCodeTranslationFactory;
    }


    /**
     * Method will return the list of User  information.
     * @return List<User>
     */
    @Override
    public CompletableFuture<List<User>> getUserList() {
        final User[] users = webClient.get()
                .uri(usersListServiceUrl)
                .headers(httpHeaders -> httpHeaders.add(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, response ->onStatus4XXCallBack(response,
                        errorCodeTranslationFactory.getErrorCodeTranslationService(ServiceEnum.USER_LIST)))
                .bodyToMono(User[].class)
                .block();
        return CompletableFuture.completedFuture(Arrays.asList(Optional.ofNullable(users).orElse(new User[]{})));
    }


}
