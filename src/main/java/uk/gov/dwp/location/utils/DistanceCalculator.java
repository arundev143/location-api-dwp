package uk.gov.dwp.location.utils;

import groovy.lang.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.util.SloppyMath;
import uk.gov.dwp.location.openapi.model.User;


@Singleton
@Slf4j
public class DistanceCalculator {
    /**
     * latitude value for london.
     */
    private static final double LATITUDE_LONDON = 51.509865d;
    /**
     * longitude value for london.
     */
    private static final double LONGITUDE_LONDON = -0.118092d;
    /**
     *  1 meter to mile converting factor.
     */
    private static final double ONE_METE_IN_MILE = 0.00062137d;

    /**
     * Method used to calculate user is with in 50 miles radius of London.
     * @param user - user object.
     * @return boolean.
     */
    public static boolean distanceFromLondonUnder50Miles(final User user) {
        final double distanceInMeters = SloppyMath
                .haversinMeters(LATITUDE_LONDON,
                        LONGITUDE_LONDON,
                        user.getLatitude().doubleValue(),
                        user.getLongitude().doubleValue());

        final double miles = distanceInMeters * ONE_METE_IN_MILE;
        if (miles <= 50d) {
            log.info(" ------  Converted Miles = " + miles);
        }
        return miles <= 50d;
    }
}
