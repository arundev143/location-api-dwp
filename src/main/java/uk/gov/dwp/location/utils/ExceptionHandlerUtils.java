package uk.gov.dwp.location.utils;

import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.openapi.model.JSONAPIErrorDetails;


public final class ExceptionHandlerUtils {

    private ExceptionHandlerUtils() {
    }

    /**
     * build JSONAPIErrorDetails.
     *
     * @param errorCode - app error code
     * @param details - details
     * @param title - title of exception
     * @param status - http status
     * @return JSONAPIErrorDetails
     */
    public static JSONAPIErrorDetails buildErrorDetails(
            final Integer errorCode,
            final String details,
            final String title,
            final HttpStatus status) {
        JSONAPIErrorDetails jsonapiErrorDetails =
                createJsonApiErrorDetails(status, errorCode, title, details);
        return jsonapiErrorDetails;
    }

    /**
     * build JSONAPIErrorDetails.
     *
     * @param status - http status
     * @param errorCode - app error code
     * @param title - title if any
     * @param details - details.
     * @return JSONAPIErrorDetails
     */
    private static JSONAPIErrorDetails createJsonApiErrorDetails(
            final HttpStatus status,
            final Integer errorCode,
            final String title,
            final String details) {
        JSONAPIErrorDetails jsonapiErrorDetails = new JSONAPIErrorDetails();
        jsonapiErrorDetails.setStatus(String.valueOf(status.value()));
        jsonapiErrorDetails.setCode(String.valueOf(errorCode));
        jsonapiErrorDetails.setTitle(title);
        jsonapiErrorDetails.setDetail(details);
        return jsonapiErrorDetails;
    }
}
