package uk.gov.dwp.location;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;


@Slf4j
@SpringBootApplication
public class LocationApiApplication {

    public static void main(final String[] args) {
        log.info("Starting Location-API...");
        SpringApplication.run(LocationApiApplication.class, args);
    }

    @PostConstruct
    public void init(){
        log.info("Setting Spring Boot SetTimeZone UTC for Location-API...");
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}
