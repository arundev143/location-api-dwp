package uk.gov.dwp.location.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ErrorDto {

    /**
     * attribute for message.
     */
    private String message;
    /**
     * attribute for title.
     */
    private String title;
    /**
     * attribute for http status code.
     */
    private int statusCode;

    /**
     * attribute for storing app error code.
     */
    private Integer errorCode;
    /**
     * attribute for storing url.
     */
    private String url;

    /**
     * attribute for storing  service name.
     */
    private String serviceName;

}
