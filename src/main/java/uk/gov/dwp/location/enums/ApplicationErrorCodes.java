package uk.gov.dwp.location.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApplicationErrorCodes {

    /**
     * User service error - guid not found error.
     */
    USER_SERVICE_NOT_FOUND(1000),

    /**
     * User service error - bad request.
     */
    USER_SERVICE_BAD_REQUEST(1001),

    /**
     * User service error - unavailable.
     */
    USER_SERVICE_UNAVAILABLE(1099),

    /**
     * Users list service error - guid not found error.
     */
    USER_LIST_SERVICE_NOT_FOUND(2000),

    /**
     * Users list service error - bad request.
     */
    USER_LIST_SERVICE_BAD_REQUEST(2001),

    /**
     * Users list service error - unavailable.
     */
    USER_LIST_SERVICE_UNAVAILABLE(2099),

    /**
     * Users list service error - guid not found error.
     */
    LONDON_USERS_SERVICE_NOT_FOUND(3000),

    /**
     * Users list service error - bad request.
     */
    LONDON_USERS_SERVICE_BAD_REQUEST(3001),

    /**
     * Users list service error - unavailable.
     */
    LONDON_USERS_SERVICE_UNAVAILABLE(3099),

    /**
     * Wrong city passed.
     */
    WRONG_CITY(4000);

    /**
     * Attribute that holds the service information.
     */
    private final Integer errorCode;

}
