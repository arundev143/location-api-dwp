package uk.gov.dwp.location.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceEnum {
    /**
     * The USER_LIST service.
     */
    USER_LIST("Service: List All Users Service"),

    /**
     * The LONDON_USERS service.
     */
    LONDON_USERS("Service: List Users from London");
    /**
     * Attribute that holds the service information.
     */
    private final String service;

}
