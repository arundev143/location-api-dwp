package uk.gov.dwp.location.enums;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.dto.ErrorDto;
import uk.gov.dwp.location.error.ErrorMessage;

import java.util.Optional;

@Slf4j
public enum ErrorTypes {

    /**
     * for handling 400 errors.
     */
    BAD_REQUEST {
        @Override
        public void setErrorContext(final ErrorDto errorDto) {
            errorDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
            ErrorTypes.setTitleAndMessage(HttpStatus.BAD_REQUEST.getReasonPhrase(), errorDto);
        }
    },
    /**
     * for handling 401 errors.
     */
    UNAUTHORIZED {
        @Override
        public void setErrorContext(final ErrorDto errorDto) {
            errorDto.setStatusCode(HttpStatus.UNAUTHORIZED.value());
            ErrorTypes.setTitleAndMessage(HttpStatus.UNAUTHORIZED.getReasonPhrase(), errorDto);
        }
    },
    /**
     * for handling 403 errors.
     */
    FORBIDDEN {
        @Override
        public void setErrorContext(final ErrorDto errorDto) {
            errorDto.setStatusCode(HttpStatus.FORBIDDEN.value());
            ErrorTypes.setTitleAndMessage(HttpStatus.FORBIDDEN.getReasonPhrase(), errorDto);
        }
    },
    /**
     * for handling 404 errors.
     */
    NOT_FOUND {
        @Override
        public void setErrorContext(final ErrorDto errorDto) {
            errorDto.setStatusCode(HttpStatus.NOT_FOUND.value());
            ErrorTypes.setTitleAndMessage(HttpStatus.NOT_FOUND.getReasonPhrase(), errorDto);
        }
    },

    /**
     * for errors that are not mapped in the application.
     */
    UNHANDLED_ERRORS {
        @Override
        public void setErrorContext(final ErrorDto errorDto) {
            ErrorTypes.setTitleAndMessage(
                    HttpStatus.valueOf(errorDto.getStatusCode()).getReasonPhrase(), errorDto);
        }
    };

    /**
     * Method will return the enum if found or return the default.
     *
     * @param name of the enum.
     * @return ErrorTypes
     */
    public static ErrorTypes error(final String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException ex) {
            log.error("Unhandled error scenario : " + name);
        }
        return ErrorTypes.UNHANDLED_ERRORS;
    }

    /**
     * Abstract method for error types.
     *
     * @param errorDto - attribute that contains the error info.
     */
    public abstract void setErrorContext(final ErrorDto errorDto);


    /**
     * private - method will create the ErrorDto object.
     *
     * @param defaultTitle - if any
     * @param errorDto     - error data collected.
     */
    private static void setTitleAndMessage(final String defaultTitle, final ErrorDto errorDto) {
        errorDto.setMessage(setDefault(errorDto.getMessage(),
                String.format(ErrorMessage.DEFAULT_MESSAGE_TEMPLATE.getMsg(), errorDto.getServiceName())));
        errorDto.setTitle(setDefault(errorDto.getTitle(), defaultTitle));
    }

    /**
     * Setting the default title/message if the title or message missing.
     *
     * @param value         - default value  if present
     * @param optionalValue - optional value to return if the default is missing.
     * @return String
     */
    private static String setDefault(final String value, final String optionalValue) {
        return Optional.ofNullable(value)
                .filter(StringUtils::isNotEmpty)
                .filter(StringUtils::isNotBlank)
                .orElse(optionalValue);
    }

}
