package uk.gov.dwp.location.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uk.gov.dwp.location.openapi.api.V1Api;
import uk.gov.dwp.location.openapi.model.LocationResponseJSONAPI;
import uk.gov.dwp.location.service.AsyncService;

@RestController
@Slf4j
public class LocationController implements V1Api {


    /**
     * The asyncService.
     */
    private final AsyncService asyncService;

    public LocationController(final AsyncService asyncService) {
        this.asyncService = asyncService;
    }


    /**
     * Method will call the async service to call the different service async manner.
     *
     * @param city - citi - optional value to be passed
     * @return ResponseEntity<LocationResponseJSONAPI>
     */
    @Override
    public ResponseEntity<LocationResponseJSONAPI> getLondonUsers(final String contentType, final String city) {
        log.info("-------- Request received : {}", city);
        return asyncService.getUsersList(city);
    }
}
