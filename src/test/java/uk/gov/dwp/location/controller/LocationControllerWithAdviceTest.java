package uk.gov.dwp.location.controller;

import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import uk.gov.dwp.location.error.GlobalExceptionHandler;
import uk.gov.dwp.location.error.LocationApiException;
import uk.gov.dwp.location.service.AsyncService;
import uk.gov.dwp.location.service.ListUsersService;
import uk.gov.dwp.location.service.impl.AsyncServiceImpl;
import uk.gov.dwp.location.utils.TestUtils;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = LocationController.class)
@Tag("unit-test")
class IncomeControllerWithAdviceTest {

    public static final String GET_LOCATION_URI = "/v1/location?city=London";
    public static final String GET_LOCATION_URI_INVALID = "/v1/location?city=Bristol";

    @MockBean
    private AsyncService asyncService;

    @Mock
    @Qualifier("listUsers")
    private ListUsersService usersService;

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private LocationApiException locationApiException;

    @BeforeEach
    void setUp() {
        asyncService = new AsyncServiceImpl(usersService, usersService);
        mockMvc = MockMvcBuilders.standaloneSetup(new LocationController(asyncService))
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();
    }

    @ParameterizedTest
    @DisplayName("Should Return User List On Valid Request.")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    void shouldReturnUserListOnValidRequest(final String filePath) throws Exception {
        when(usersService.getUserList()).thenReturn(CompletableFuture.completedFuture(Arrays.asList(TestUtils.getUserArray(filePath))));
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_LOCATION_URI)
                .header(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /* Testing errors */

    @ParameterizedTest
    @DisplayName("Should Return http 404 on invalid Request.")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    void shouldReturn404OnInvalidRequest(final String filePath) throws Exception {
        when(usersService.getUserList()).thenReturn(CompletableFuture.completedFuture(Arrays.asList(TestUtils.getUserArray(filePath))));
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_LOCATION_URI_INVALID)
                .header(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    @DisplayName("Should Return Bad Request On Missing Headers")
    void shouldReturnBadRequestOnMissingHeadersInRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_LOCATION_URI)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}


