package uk.gov.dwp.location.factory;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.error.ErrorCodesConfig;
import uk.gov.dwp.location.error.ServiceException;
import uk.gov.dwp.location.service.ErrorCodeTranslationService;
import uk.gov.dwp.location.service.impl.ListUsersErrorServiceImpl;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@Tag("unit-test")
public class ErrorCodeTranslationFactoryTest {

    private ErrorCodeTranslationFactory factory = null;

    @Mock
    private ErrorCodesConfig errorCodesConfig;

    @BeforeEach
    void setUp() {
        Set<ErrorCodeTranslationService> errorServiceSet = Stream.of(
                new ListUsersErrorServiceImpl(errorCodesConfig))
                .collect(Collectors.toSet());
        log.info("size : {}",errorServiceSet.size());
        factory = new ErrorCodeTranslationFactory(errorServiceSet);
    }

    @Test
    @DisplayName("Should throw error when identity service implementation not found")
    void shouldThrowErrorWhenServiceImplementationNotFound() {
        factory = new ErrorCodeTranslationFactory(Collections.emptySet());
        EnumSet.allOf(ServiceEnum.class).forEach(consumer -> {
            ServiceException serviceException = assertThrows(ServiceException.class,
                    () -> factory.getErrorCodeTranslationService(consumer));
            assertThat(serviceException.getMessage(), is(equalTo("Error code translation service not found")));
        });

    }
}
