package uk.gov.dwp.location.features;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpHeaders;

import java.util.Arrays;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

@Slf4j
public class LocationControllerStepDefs {

    private RequestSpecification given;
    private Response actualResponse;

    @After
    public void init() {
        this.given = null;
        actualResponse = null;
    }
    @Given("{string} as basic scenario")
    public void as_basic_scenario(String string) {
        this.given = given().header(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
    }


    @When("client call the api with urlTemplate {string}")
    public void client_call_the_api_with_url_template(String string) {
      this.actualResponse =  this.given.log().all().when().get(string);
    }

    @Then("the client receives status code of {int} from the stub and correct status code is {int}")
    public void the_client_receives_status_code_of_from_the_stub_and_correct_status_code_is(Integer int1, Integer int2) {
        this.actualResponse
                .then()
                .statusCode(int1);
    }

}
