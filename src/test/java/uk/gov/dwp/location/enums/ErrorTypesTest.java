package uk.gov.dwp.location.enums;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.dto.ErrorDto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Tag("unit-test")
class ErrorTypesTest {

    @Test
    @DisplayName("When 400 - BAD REQUEST")
    void shouldUpdateErrorDtoForHttp400() {

        final ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message("message")
                .title("title")
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.valueOf(HttpStatus.BAD_REQUEST.name()).setErrorContext(errorDto);
        assertEquals("message", errorDto.getMessage());
        assertEquals("title", errorDto.getTitle());
        assertEquals(400, errorDto.getStatusCode());
    }

    @Test
    @DisplayName("When 400 - BAD REQUEST - default title and message")
    void shouldUpdateErrorDtoForHttp400WithDefaultTitleAndMessage() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.BAD_REQUEST.name()).setErrorContext(errorDto);
        assertNotEquals("message", errorDto.getMessage());
        assertNotEquals("title", errorDto.getTitle());
        assertEquals(400, errorDto.getStatusCode());
    }

    @Test
    @DisplayName("When 404 - NOT FOUND")
    void shouldUpdateErrorDtoForHttp404NotFound() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .message("message")
                .title("title")
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.NOT_FOUND.name()).setErrorContext(errorDto);
        assertEquals("message", errorDto.getMessage());
        assertEquals("title", errorDto.getTitle());
        assertEquals(404, errorDto.getStatusCode());
    }

    @Test
    @DisplayName("When 404 - BAD REQUEST - default title and message")
    void shouldUpdateErrorDtoForHttp404WithDefaultTitleAndMessage() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.NOT_FOUND.name()).setErrorContext(errorDto);
        assertNotEquals("message", errorDto.getMessage());
        assertNotEquals("title", errorDto.getTitle());
        assertEquals(404, errorDto.getStatusCode());
    }

    @Test
    @DisplayName("When 422 - UNPROCESSABLE ENTITY")
    void shouldUpdateErrorDtoForHttp422() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .message("message")
                .title("title")
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.UNPROCESSABLE_ENTITY.name()).setErrorContext(errorDto);
        assertEquals("message", errorDto.getMessage());
        assertEquals("title", errorDto.getTitle());
        assertEquals(422, errorDto.getStatusCode());
    }

    @Test
    @DisplayName("When 422 - UNPROCESSABLE ENTITY - default title and message")
    void shouldUpdateErrorDtoForHttp422WithDefaultTitleAndMessage() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.UNPROCESSABLE_ENTITY.name()).setErrorContext(errorDto);
        assertNotEquals("message", errorDto.getMessage());
        assertNotEquals("title", errorDto.getTitle());
        assertEquals(422, errorDto.getStatusCode());
    }



    @Test
    @DisplayName("When Unknown error occurred.")
    void shouldUpdateErrorDtoWhenHttpUnknown() {

        ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.PROCESSING.value())
                .url("https://google.com")
                .serviceName(ServiceEnum.LONDON_USERS.name())
                .build();
        ErrorTypes.error(HttpStatus.PROCESSING.name()).setErrorContext(errorDto);
        assertEquals("Actual error message is empty - LONDON_USERS", errorDto.getMessage());
        assertEquals("Processing", errorDto.getTitle());
        assertEquals(102, errorDto.getStatusCode());
    }

}
