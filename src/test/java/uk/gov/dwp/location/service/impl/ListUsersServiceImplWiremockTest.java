package uk.gov.dwp.location.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.util.ReflectionTestUtils;
import uk.gov.dwp.location.enums.ServiceEnum;
import uk.gov.dwp.location.error.LocationApiException;
import uk.gov.dwp.location.openapi.model.User;
import uk.gov.dwp.location.service.ListUsersService;
import uk.gov.dwp.location.utils.TestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@SpringBootTest
@Tag("unit-test")
public class ListUsersServiceImplWiremockTest {
    private static final String USERS_SERVICE_URL = "http://localhost:8446/users-404";
    private static WireMockServer wireMockServer;

    @Autowired
    private ResourceLoader resourceLoader;

   //@Autowired

    private final ListUsersService listUsersService;

    @Autowired
    public ListUsersServiceImplWiremockTest(@Qualifier("listUsers") ListUsersService londonUsersService){
        this.listUsersService = londonUsersService;
    }

    @BeforeAll
    public static void setup() {
        wireMockServer = new WireMockServer(options().usingFilesUnderClasspath("wiremock").port(8446));
        wireMockServer.start();
    }

    @AfterAll
    public static void teardown() {
        wireMockServer.resetAll();
        wireMockServer.stop();
    }

    @ParameterizedTest
    @DisplayName("Should Return Valid list of user Array who stay close to london using Wiremock")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    public void retrieveListOfLondonUsers(final String filePath) throws ExecutionException, InterruptedException, JsonProcessingException {
            CompletableFuture<List<User>> future = listUsersService.getUserList();
            List<User> userList = future.get();
            log.info("------- User List : {}", userList.size());
            List<User> expectedUserList = Arrays.asList(TestUtils.getUserArray(filePath));
            assertEquals(expectedUserList.size(), userList.size());
    }

    @Test
    @DisplayName("Should Return NotFoundException Exception - Http 400")
    void shouldReturnLocationAppException_HTTP404() {
        ReflectionTestUtils.setField(listUsersService, "usersListServiceUrl", USERS_SERVICE_URL);
        final LocationApiException ex = assertThrows(LocationApiException.class, () -> listUsersService.getUserList().get());
        assertEquals(404, ex.getHttpStatusCode());
        assertEquals(ServiceEnum.USER_LIST.name(), ex.getServiceName());
    }
}
