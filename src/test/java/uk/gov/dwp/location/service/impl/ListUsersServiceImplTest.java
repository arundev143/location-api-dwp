package uk.gov.dwp.location.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import uk.gov.dwp.location.factory.ErrorCodeTranslationFactory;
import uk.gov.dwp.location.openapi.model.User;
import uk.gov.dwp.location.utils.TestUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag("unit-test")
@Slf4j
public class ListUsersServiceImplTest {

    private static final String USERS_SERVICE_URL = "http://localhost:8445/users/";

    @Mock
    WebClient dwpWebClient;

    @Mock
    private WebClient.RequestHeadersSpec requestHeadersMock;

    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriMock;

    @Mock
    private WebClient.ResponseSpec responseSpecMock;

    @Mock
    private ErrorCodeTranslationFactory factory;

    private ListUsersServiceImpl usersService;

    @BeforeEach
    void setUp() {
        dwpWebClient = Mockito.mock(WebClient.class);
        usersService = new ListUsersServiceImpl(dwpWebClient, factory);
    }

    @ParameterizedTest
    @DisplayName("Should get list of users in London")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    void shouldReturnListOfLondonUsers(String filepath)throws JsonProcessingException, ExecutionException, InterruptedException{
        log.info("File path : {}",filepath);
        ReflectionTestUtils.setField(usersService, "usersListServiceUrl", USERS_SERVICE_URL);
        when(dwpWebClient.get()).thenReturn(requestHeadersUriMock);
        when(requestHeadersMock.headers(any())).thenReturn(requestHeadersMock);
        when(requestHeadersUriMock.uri(USERS_SERVICE_URL)).thenReturn(requestHeadersMock);
        when(requestHeadersMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.onStatus(any(), any())).thenReturn(responseSpecMock);
        when(responseSpecMock.bodyToMono(User[].class)).thenReturn(Mono.just(TestUtils.getUserArray(filepath)));

        CompletableFuture<List<User>> future = usersService.getUserList();

        List<User> userList = future.get();
        log.info("User List size : {}",userList.size());
        assertNotNull(future);
        assertEquals(1000,userList.size());

    }







    /**
     * Method will create list of Users.
     * @return List<User>
     */
    private List<User> createUsersList(){
        List<User> users = new ArrayList<>();
        for(int i =0 ; i<10 ; i++ ){
            User user = new User();
            user.setLongitude(new BigDecimal(i+"0.12345"));
            user.setLatitude(new BigDecimal("-"+i+"0.12345"));
            user.firstName("First-Name-"+i);
            user.lastName("Last-name-"+i);
            user.email("user-"+i+"@dwp.gov.uk");
            users.add(user);
        }
        return users;
    }

    /**
     * Method will create User Array.
     * @return User[]
     */
    private User[] createUsersArray(){
        List<User> users = createUsersList();
        User[] usersArray = new User[users.size()];
        return users.toArray(usersArray);

    }
}
