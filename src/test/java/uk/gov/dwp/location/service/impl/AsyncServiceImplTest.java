package uk.gov.dwp.location.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import uk.gov.dwp.location.error.LocationApiException;
import uk.gov.dwp.location.openapi.model.LocationResponseJSONAPI;
import uk.gov.dwp.location.service.ListUsersService;
import uk.gov.dwp.location.utils.TestUtils;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag("unit-test")
public class AsyncServiceImplTest {
    @Mock
    @Qualifier("listUsers")
    private ListUsersService usersService;

    private AsyncServiceImpl asyncService;

    @BeforeEach
    void setUp() {
        asyncService = new AsyncServiceImpl(usersService, usersService);
    }

    @ParameterizedTest
    @DisplayName("Should return user list from for valid request for london only")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    public void shouldReturnDataOnValidLondonOnlyResponse(final String filePath) throws JsonProcessingException {
        when(usersService.getUserList()).thenReturn(CompletableFuture.completedFuture(Arrays.asList(TestUtils.getUserArray(filePath))));
        ResponseEntity<LocationResponseJSONAPI> responseJSONAPIResponseEntity =  asyncService.getUsersList("London");
        assertNotNull(responseJSONAPIResponseEntity);
    }

    @ParameterizedTest
    @DisplayName("Should return user list from for valid request for all")
    @ValueSource(strings = {
            "/wiremock/__files/users/valid-users.json"
    })
    public void shouldReturnDataOnValidAllResponse(final String filePath) throws JsonProcessingException {
        when(usersService.getUserList()).thenReturn(CompletableFuture.completedFuture(Arrays.asList(TestUtils.getUserArray(filePath))));
        ResponseEntity<LocationResponseJSONAPI> responseJSONAPIResponseEntity =  asyncService.getUsersList(null);
        assertNotNull(responseJSONAPIResponseEntity);
    }

    @Test
    @DisplayName("Should return exception when we send a city other than london")
    void shouldReturnLocationAppException() throws JsonProcessingException {
        final LocationApiException ex = assertThrows(LocationApiException.class, () -> asyncService.getUsersList("Bristol"));
        assertEquals(4000, ex.getErrorCode());
    }
}
