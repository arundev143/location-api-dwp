package uk.gov.dwp.location.error;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.dto.ErrorDto;

import static org.junit.jupiter.api.Assertions.*;

@Tag("unit-test")
class LocationApiExceptionTest {

    public static final String FIELD_NAME = "NINO";
    public static final String ERROR_MSG = "Not Found";
    public static final String ERROR_TITLE = "Error - Not Found";


    @Test
    @DisplayName("Should be NotFoundException thrown")
    void shouldHandleExceptionWhenNoNinoIsFound() {
        final ErrorDto errorDto = ErrorDto.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .message(ERROR_MSG)
                .title(ERROR_TITLE)
                .errorCode(1000)
                .build();

        final LocationApiException notFoundException = new LocationApiException(errorDto);
        assertEquals(ERROR_MSG, notFoundException.getMessage());
        assertEquals(HttpStatus.NOT_FOUND.value(), notFoundException.getHttpStatusCode());
    }
}
