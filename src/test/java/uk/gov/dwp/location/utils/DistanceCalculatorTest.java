package uk.gov.dwp.location.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import uk.gov.dwp.location.openapi.model.User;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import java.math.BigDecimal;

public class DistanceCalculatorTest {

    @Test
    @DisplayName("Should return true when distance less than 50 miles from london")
    public void testDistanceUnder50ReturnTrue(){
        User user = new User();
        user.setLatitude(new BigDecimal("51.509865"));
        user.setLongitude(new BigDecimal("-0.118092"));
        boolean under50Miles = DistanceCalculator.distanceFromLondonUnder50Miles(user);
        assertThat(under50Miles,is(equalTo(true)));

    }

    @Test
    @DisplayName("Should return false when distance more than 50 miles from london")
    public void testDistanceMoreThan50ReturnFalse(){
        User user = new User();
        user.setLatitude(new BigDecimal("51.568535"));
        user.setLongitude(new BigDecimal("-1.772232"));
        boolean under50Miles = DistanceCalculator.distanceFromLondonUnder50Miles(user);
        assertThat(under50Miles,is(equalTo(false)));

    }
}
