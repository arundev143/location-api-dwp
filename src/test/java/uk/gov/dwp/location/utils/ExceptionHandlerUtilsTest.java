package uk.gov.dwp.location.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import uk.gov.dwp.location.openapi.model.JSONAPIErrorDetails;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

class ExceptionHandlerUtilsTest {

    @Test
    @DisplayName("Should return error jsonapiErrorDetails response for bad request")
    void buildErrorDetails() {
        int errorCode = 4001;
        String details = "Bad request provided";
        String title = "Bad request";
        HttpStatus status = HttpStatus.BAD_REQUEST;

        JSONAPIErrorDetails jsonapiErrorDetails
                = ExceptionHandlerUtils.buildErrorDetails(errorCode, details, title, status);

        assertThat(jsonapiErrorDetails.getStatus(), is(String.valueOf(HttpStatus.BAD_REQUEST.value())));
        assertThat(jsonapiErrorDetails.getCode(), is(equalTo(String.valueOf(errorCode))));
        assertThat(jsonapiErrorDetails.getTitle(), is(equalTo(title)));
        assertThat(jsonapiErrorDetails.getDetail(), is(equalTo(details)));

    }

}
