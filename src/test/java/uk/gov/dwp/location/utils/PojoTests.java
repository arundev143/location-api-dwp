package uk.gov.dwp.location.utils;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.regex.Pattern;
@Tag("unit-test")
public class PojoTests {

    private static final String MODEL_PACKAGE = "uk.gov.dwp.location.dto";

    private static final Pattern TEST_CLASS_NAME_PATTERN = Pattern.compile("^\\S+?Test(?:\\$\\S+)?$");

    private static final Pattern BUILDER_CLASS_NAME_PATTERN = Pattern.compile("^\\S+?Builder$");

    private static final PojoClassFilter BASE_CLASS_FILTER = pojoClass -> pojoClass.isConcrete()
            && !TEST_CLASS_NAME_PATTERN.matcher(pojoClass.getName()).matches()
            && !BUILDER_CLASS_NAME_PATTERN.matcher(pojoClass.getName()).matches();

    List<PojoClass> pojoClasses;

    @BeforeEach
    void setUp() {
        pojoClasses = PojoClassFactory.getPojoClassesRecursively(MODEL_PACKAGE, BASE_CLASS_FILTER);
    }

    @Test
    public void testPojoStructureAndBehavior() {

        Validator validator = ValidatorBuilder.create()
                .with(new SetterMustExistRule(),
                        new GetterMustExistRule())
                .with(new GetterTester())
                .with(new SetterTester())
                .build();
        validator.validate(pojoClasses);

        validator = ValidatorBuilder.create()
                .with(new SetterTester())
                .build();

        validator.validate(pojoClasses);
    }
}
