package uk.gov.dwp.location.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import uk.gov.dwp.location.openapi.model.User;

import java.nio.charset.StandardCharsets;

@Slf4j
public class TestUtils {

   private static final ObjectMapper objectMapper;

    static {
        objectMapper = Jackson2ObjectMapperBuilder.json().build();
        objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
    }

    public static User[] getUserArray(String filePath) throws JsonProcessingException {
        return objectMapper.readValue(
                IOUtils.toString(User[].class.getResourceAsStream(filePath),
                        StandardCharsets.UTF_8), User[].class);
    }
}
