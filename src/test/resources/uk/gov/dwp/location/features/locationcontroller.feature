@regression
Feature: Wiremock the Income controller Integration test

  Scenario Outline: Validating various responses from Location API
    Given <description> as basic scenario
    When client call the api with urlTemplate <urlTemplate>
    Then the client receives status code of <expectedHttpStatus> from the stub and correct status code is <httpStatusCode>
    Examples:
      | description                                               | httpStatusCode | urlTemplate                                      | expectedHttpStatus |
      | "All users including users in London and close to London" | 200            | "http://localhost:8082/v1/location"              | 200                |
      | "All users including users in London only"                | 200            | "http://localhost:8082/v1/location?city=London"  | 200                |
      | "All users including users in Bristol "                   | 404            | "http://localhost:8082/v1/location?city=Bristol" | 404                |